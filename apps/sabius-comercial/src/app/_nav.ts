export const navItems = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer'    
  },    
  {
    name: 'Beneficiários',
    url: '/beneficiarios',
    icon: 'fas fa-users',
    children: [
      {
        name: 'Cadastro Beneficiário',
        url: '/beneficiarios/cadastrobeneficiario',
        icon: 'fas fa-angle-right'
      }
    ]
  },
  {      
    name: 'Empresas',
    url: '/empresas',
    icon: 'fas fa-building',
    children: [
      {
        name: 'Cadastro Empresa',
        url: '/empresas/cadastroempresa',
        icon: 'fas fa-angle-right'
      }
    ]
  },
  {      
    name: 'Contratos',
    url: '/contrato',
    icon: 'fas fa-file-signature',
    children: [
      {
        name: 'Cadastro Contrato',
        url: '/empresas/cadastroempresa',
        icon: 'fas fa-angle-right'        
      },
      {
        name: 'Cópia de Contrato',
        url: '/empresas/cadastroempresa',
        icon: 'fas fa-angle-right'
      }
    ]
  },
  {      
    name: 'Dados Gerais',
    url: '/dadosgerais',    
    icon: 'fas fa-book-reader',    
    children: [
      {
        name: 'Alteração de Beneficiáro',
        url: '/empresas/cadastroempresa',
        icon: 'fas fa-angle-right'
      },
      {
        name: 'Alteração de Plano',
        url: '/empresas/cadastroempresa',
        icon: 'fas fa-angle-right'
      },
      {
        name: 'Transferência de Contrato',
        url: '/empresas/cadastroempresa',
        icon: 'fas fa-angle-right'
      },
      {
        name: 'Histôricos',
        url: '/historico',        
        icon: 'fas fa-angle-right',
        children: [
          {
            name: 'Histôrico de Mudança de Plano',
            url: '/empresas/cadastroempresa',
            icon: 'fas fa-angle-right'
          },
          {
            name: 'Histôrico de Mudança de Contrato',
            url: '/empresas/cadastroempresa',
            icon: 'fas fa-angle-right'
          },
          {
            name: 'Relatôrios',
            url: '/relatorios',            
            icon: 'fas fa-angle-right',
            children: [
              {
                name: 'Relatório de Histôrico de Plano',
                url: '/empresas/cadastroempresa',
                icon: 'fas fa-angle-right'
              },
              {
                name: 'Relatório de Histôrico de Contrato',
                url: '/empresas/cadastroempresa',
                icon: 'fas fa-angle-right'
              }
            ]
          }
        ]
      }
    ]
  }  
];
