import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { formatDate, DatePipe } from '../../../../../../node_modules/@angular/common';
import { Timestamp } from '../../../../../../node_modules/rxjs/internal-compatibility';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  
  arrGraficLine: any;
  arrGraficBar: any;
  arrGraficDoughnut: any;

  populaGrafic() {

    this.arrGraficLine = {
      labels: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Juho', 'Julho'],
      datasets: [
          {
              label: 'Solicitações Cadastradas',
              data: [40, 59, 62, 55, 66, 57, 70],              
              fill: false,
              borderColor: '#3169F4'
          },
          {
              label: 'Solicitações Pendentes',
              data: [20, 25, 30, 40, 32, 49, 47],
              fill: false,
              borderColor: '#F18938'
          },
          {
            label: 'Solicitações Atualizadas',
            data: [22, 32, 42, 40, 55, 60, 62],
            fill: false,            
            borderColor: '#0B710C'
          },
          {
            label: 'Solicitações Canceladas',
            data: [10, 8, 9, 15, 13, 17, 8],
            fill: false,            
            borderColor: '#EE0E0E'
          }
      ]
    }

    ////////////////
    this.arrGraficBar = {
      labels: ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'],
      datasets: [
          {
              label: 'Quantidade de vidas enviadas a ANS (2018)',
              backgroundColor: '#344E84',
              borderColor: '#FFF',
              data: [2165, 2159, 2180, 2181, 2156, 2155, 2154]
          }
      ]
    };

    /////////////////
    this.arrGraficDoughnut = {
      labels: ['Reativações', 'Bloqueios','Exclusões'],
      datasets: [
          {
              data: [30, 15, 10],
              backgroundColor: [
                  "#07A20E",
                  "#344E84",
                  "#BA0707"
              ],
              hoverBackgroundColor: [
                  "#07A20E",
                  "#344E84",
                  "#BA0707"
              ]
          }]    
    };    

  }  

  radioModel = 'Month';

  //widget1
  public lineChart1Data: Array<any> = [
    {
      data: [40, 59, 62, 55, 66, 57, 70],
      label: 'Solicitações Cadastradas'
    }
  ];
  public lineChart1Labels: Array<any> = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Juho', 'Julho'];
  public lineChart1Options: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent'
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent',
        }

      }],
      yAxes: [{
        display: false,
        ticks: {
          display: false,
          min: 40 - 5,
          max: 84 + 5,
        }
      }],
    },
    elements: {
      line: {
        borderWidth: 1
      },
      point: {
        radius: 4,
        hitRadius: 10,
        hoverRadius: 4,
      },
    },
    legend: {
      display: false
    }
  };
  public lineChart1Colours: Array<any> = [
    {
      backgroundColor: getStyle('--primary'),
      borderColor: '#3169F4'
    }
  ];
  public lineChart1Legend = false;
  public lineChart1Type = 'line';

  //widget2
  public lineChart2Data: Array<any> = [
    {
      data: [20, 25, 30, 40, 32, 49, 47],
      label: 'Solicitações Pendentes'
    }
  ];
  public lineChart2Labels: Array<any> = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Juho', 'Julho'];
  public lineChart2Options: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent'
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent',
        }

      }],
      yAxes: [{
        display: false,
        ticks: {
          display: false,
          min: 1 - 5,
          max: 34 + 5,
        }
      }],
    },
    elements: {
      line: {
        tension: 0.00001,
        borderWidth: 1
      },
      point: {
        radius: 4,
        hitRadius: 10,
        hoverRadius: 4,
      },
    },
    legend: {
      display: false
    }
  };
  public lineChart2Colours: Array<any> = [
    {
      backgroundColor: getStyle('--info'),
      borderColor: '#F18938'
    }
  ];
  public lineChart2Legend = false;
  public lineChart2Type = 'line';


  //widget3
  public lineChart3Data: Array<any> = [
    {
      data: [22, 32, 42, 40, 55, 60, 62],
      label: 'Solicitações Atualizadas'
    }
  ];
  public lineChart3Labels: Array<any> = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Juho', 'Julho'];
  public lineChart3Options: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        display: false
      }],
      yAxes: [{
        display: false
      }]
    },
    elements: {
      line: {
        borderWidth: 2
      },
      point: {
        radius: 0,
        hitRadius: 10,
        hoverRadius: 4,
      },
    },
    legend: {
      display: false
    }
  };
  public lineChart3Colours: Array<any> = [
    {
      backgroundColor: '#0B710C',
      //borderColor: '#0B710C',
    }
  ];
  public lineChart3Legend = false;
  public lineChart3Type = 'line';


  //widget4
  public barChart1Data: Array<any> = [
    {
      data: [10, 8, 9, 15, 13, 17, 8],
      label: 'Solicitações Canceladas'
    }
  ];
  public barChart1Labels: Array<any> = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Juho', 'Julho'];
  public barChart1Options: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        display: false,
        barPercentage: 0.6,
      }],
      yAxes: [{
        display: false
      }]
    },
    legend: {
      display: false
    }
  };
  public barChart1Colours: Array<any> = [
    {
      backgroundColor: '#EE0E0E',
      borderWidth: 0
    }
  ];
  public barChart1Legend = false;
  public barChart1Type = 'bar';

  // mainChart
  public mainChartElements = 27;
  public mainChartData1: Array<number> = [];
  public mainChartData2: Array<number> = [];
  public mainChartData3: Array<number> = [];

  public mainChartData: Array<any> = [
    {
      data: this.mainChartData1,
      label: 'Current'
    },
    {
      data: this.mainChartData2,
      label: 'Previous'
    },
    {
      data: this.mainChartData3,
      label: 'BEP'
    }
  ];
  /* tslint:disable:max-line-length */
  public mainChartLabels: Array<any> = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Monday', 'Thursday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  /* tslint:enable:max-line-length */
  public mainChartOptions: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips,
      intersect: true,
      mode: 'index',
      position: 'nearest',
      callbacks: {
        labelColor: function(tooltipItem, chart) {
          return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
        }
      }
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        gridLines: {
          drawOnChartArea: false,
        },
        ticks: {
          callback: function(value: any) {
            return value.charAt(0);
          }
        }
      }],
      yAxes: [{
        ticks: {
          beginAtZero: true,
          maxTicksLimit: 5,
          stepSize: Math.ceil(250 / 5),
          max: 250
        }
      }]
    },
    elements: {
      line: {
        borderWidth: 2
      },
      point: {
        radius: 0,
        hitRadius: 10,
        hoverRadius: 4,
        hoverBorderWidth: 3,
      }
    },
    legend: {
      display: false
    }
  };
  public mainChartColours: Array<any> = [
    { // brandInfo
      backgroundColor: hexToRgba(getStyle('--info'), 10),
      borderColor: getStyle('--info'),
      pointHoverBackgroundColor: '#fff'
    },
    { // brandSuccess
      backgroundColor: 'transparent',
      borderColor: getStyle('--success'),
      pointHoverBackgroundColor: '#fff'
    },
    { // brandDanger
      backgroundColor: 'transparent',
      borderColor: getStyle('--danger'),
      pointHoverBackgroundColor: '#fff',
      borderWidth: 1,
      borderDash: [8, 5]
    }
  ];
  public mainChartLegend = false;
  public mainChartType = 'line';

  public random(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  ngOnInit(): void {
    // generate random values for mainChart
    for (let i = 0; i <= this.mainChartElements; i++) {
      this.mainChartData1.push(this.random(50, 200));
      this.mainChartData2.push(this.random(80, 100));
      this.mainChartData3.push(65);
      this.populaGrafic();
    }
  }
}
