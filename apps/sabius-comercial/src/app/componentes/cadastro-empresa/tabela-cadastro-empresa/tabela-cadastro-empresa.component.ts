import { NotificationService } from './../../../../../../../libs/core/src/lib/notification/notification.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { EmpresaService } from './../../../../../../../libs/core/src/lib/empresa/empresa.service';
import { Empresa } from './../../../../../../../libs/model/src/lib/empresa';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'workspace-apps-tabela-cadastro-empresa',
  templateUrl: './tabela-cadastro-empresa.component.html',
  styleUrls: ['./tabela-cadastro-empresa.component.css']
})
export class TabelaCadastroEmpresaComponent implements OnInit {

  displayDialog: boolean;
  dialogMsgDelete: boolean;

  empresa: Empresa = new Empresa();
  selectedEmpresa: Empresa;
  novaEmpresa: boolean;
  empresas: Empresa[];
  cols: any[];

  constructor(private empresaService: EmpresaService,
              private notificationService: NotificationService,
              private spinnerService: Ng4LoadingSpinnerService) { }

  ngOnInit() {    
    
    this.listaDeEmpresas();
    
    this.cols = [
      { field: 'codUnimed',    header: 'Cód Unimed' },
      { field: 'codEmpresa',   header: 'Cód Empresa' },
      { field: 'razaoSocial',  header: 'Razão Social' },
      { field: 'cnpj',         header: 'CNPJ' },
      { field: 'dataCadastro', header: 'Data Cadastro' },
      { field: 'dataExclusao', header: 'Data Exclusão' }    
    ];
  }

  listaDeEmpresas() {
    this.empresaService.listarEmpresas().subscribe(emp => {
      this.empresas = emp;
    });
  }

  exibirDialogMsgDelete(empresa) {
    this.empresa = empresa;
    this.dialogMsgDelete=true;
  }

  eventoDialogMsgDelete(event) {
    if(event==='N'){
      this.empresa = null;
      this.dialogMsgDelete=false;
    } else {
      this.spinnerService.show();
      this.empresaService.deletar(this.empresa).subscribe(()=>{
        setTimeout(()=>this.spinnerService.hide(),2000);
        this.listaDeEmpresas();
        this.notificationService.showSuccess('','Registro deletado com sucesso.');
      }, error=>{
        this.notificationService.showError('','Erro ao deletar o registro.');
      });
      this.dialogMsgDelete=false;
    }
  }

  deleteTeste(empresa) {  
    this.empresaService.deletar(empresa).subscribe(()=>{
      this.listaDeEmpresas();
    });
  }

  atualizarTeste(empresa) {    
    this.empresaService.alterar(empresa).subscribe(()=>{
      this.listaDeEmpresas();
    });
  }

  delete() {
    const index = this.findSelectedEmpresaIndex();
    this.empresas = this.empresas.filter((val, i) => i !== index);
    this.empresa = null;
    this.displayDialog = false;
  }

  rowSelect(event) {
    this.novaEmpresa = false;
    this.empresa = {...event.data};
    this.displayDialog = true;
  }

  findSelectedEmpresaIndex(): number {
    return this.empresas.indexOf(this.selectedEmpresa);
  }

  gerarRelatorioEmpresa() {    
    this.empresaService.gerarRelEmpresa().subscribe(relEmp =>{              
      //nome do arquivo e sua extenção
      const date = new Date().valueOf();
      const text = 'Relatório Empresa';                  
      const fileName = date + '.' + text + '.pdf';

      //método que retorna um blob de dataUri
      const fileBlob = this.dataURItoBlob( relEmp.file );      
      const file = new File([fileBlob], fileName, { type: 'application/pdf' });      
      const fileUrl = URL.createObjectURL(file); 
      console.log(fileUrl);     
      window.open(fileUrl);      
    })
  }

  //método que converte encode em blob
  dataURItoBlob(dataURI) {
    const byteString = atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    return new Blob([arrayBuffer], { type: 'application/pdf' });
  }

}
