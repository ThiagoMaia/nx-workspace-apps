import { DepartamentoService } from './../../../../../../libs/core/src/lib/departamento/departamento.service';
import { Departamento } from './../../../../../../libs/model/src/lib/departamento';
import { LETTERS_PATTERN } from './../../../../../../libs/util/src/lib/app.constantes';
import { UnimedProdComponent } from './../../../../../../libs/unimed-prod/src/lib/unimed-prod/unimed-prod.component';
import { EmpresaService } from './../../../../../../libs/core/src/lib/empresa/empresa.service';
import { NotificationService } from './../../../../../../libs/core/src/lib/notification/notification.service';
import { TipoPorteEmpresaService } from './../../../../../../libs/core/src/lib/tipoporteempresa/tipoporteempresa.service';
import { AppUtil } from '@workspace-apps/util';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { SelectItem } from 'primeng/api';
import { TipoPorteEmpresa } from 'libs/model/src/lib/tipoporteempresa';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { TabelaCadastroEmpresaComponent } from './tabela-cadastro-empresa/tabela-cadastro-empresa.component';
import { Empresa } from 'libs/model/src/lib/empresa';
import { Observable } from 'rxjs';

@Component({
  selector: 'workspace-apps-cadastro-empresa',
  templateUrl: './cadastro-empresa.component.html',
  styleUrls: ['./cadastro-empresa.component.css']
})
export class CadastroEmpresaComponent implements OnInit {

  cadEmpresaForm: FormGroup; 
  tipoPorteEmp: SelectItem[];  
  filterRazSoc: RegExp;
  pt: any;  

  @ViewChild(UnimedProdComponent) unimedProd: UnimedProdComponent;  
  @ViewChild(TabelaCadastroEmpresaComponent) gridEmpresa: TabelaCadastroEmpresaComponent;;

  constructor(private formBuilder: FormBuilder, 
              private tipoPorteEmpresaService: TipoPorteEmpresaService,
              private empresaService: EmpresaService,
              private notificationService: NotificationService,
              private spinnerService: Ng4LoadingSpinnerService,
              private departamentoService: DepartamentoService) { }

  ngOnInit() {
    this.cadEmpresaFormGroup();
    this.pt = AppUtil.configCalendarPtBr();
    this.listarTipoPorteEmpresa();    
    this.filterRazSoc=LETTERS_PATTERN;    
    //this.listarDepartamentos(); --testando webservice
    //this.buscarDepartamento();
  }  

  cadEmpresaFormGroup() {
    this.cadEmpresaForm = this.formBuilder.group({
      codUnimed:        [null, [Validators.required]],
      descricao:        [null, [Validators.required]],
      codEmpresa:       [null, [Validators.required]],
      razaoSocial:      [null, [Validators.required]],
      cnpj:             [null, [Validators.required]],
      dataCadastro:     [null, [Validators.required]],
      dataExclusao:     [null, [Validators.required]],
      tipoPfPj:         [null, [Validators.required]],
      tipoPorteEmpresa: [null, [Validators.required]]      
    });        
  }  

  listarTipoPorteEmpresa() {
    this.tipoPorteEmp = [
      {label:'SELECIONE UMA OPÇÃO', value:null}    
    ];
    this.tipoPorteEmpresaService.listarTipoPorteEmpresa().subscribe((porteEmpresa: TipoPorteEmpresa[]) => {
      porteEmpresa.forEach(porteEmp => {        
        this.tipoPorteEmp.push( {label:porteEmp.descPorte , value:porteEmp.tipoPorte} );
      });
    });    
  }  

  salvar() {    
    if(this.cadEmpresaForm.valid) {
      this.verficaRegitroJaCadastrado().pipe()
        .subscribe(existeEmp =>{       
            if(existeEmp.length>0 && existeEmp[0].codUnimed != null && existeEmp[0].codEmpresa != null){
              this.notificationService.showWarning('','Registro já cadastrado.');          
              return;
            } else {
              this.spinnerService.show();
              this.empresaService.salvar(this.cadEmpresaForm.value).pipe()
                .subscribe(()=>{
                  ()=>this.spinnerService.hide();                  
                  this.limpar();
                  this.gridEmpresa.listaDeEmpresas();
                  this.notificationService.showSuccess('','Registro salvo com sucesso.');
                }, error=>{
                  this.notificationService.showError('','Erro ao salvar o registro.');
                })
            }
        });
    } else {
      Object.keys(this.cadEmpresaForm.controls).forEach(campo=>{
        const controle = this.cadEmpresaForm.get(campo);
        controle.markAsDirty();        
      });      
      Object.keys(this.unimedProd.unimedForm.controls).forEach(campo=>{
        const controle = this.unimedProd.unimedForm.get(campo);
        controle.markAsDirty();        
      });      
    }         
  }

  verficaRegitroJaCadastrado(): Observable<Empresa[]> {
    let emp = new Empresa();
    emp.codUnimed = this.getCadEmpresaEmpresa.codUnimed.value;
    emp.codEmpresa = this.getCadEmpresaEmpresa.codEmpresa.value;    
    return this.empresaService.buscarPorEmpresa(emp);
  }

  limpar() {
    this.cadEmpresaForm.reset();  
    this.unimedProd.unimedForm.reset();        
    this.cadEmpresaForm.markAsUntouched();
    this.unimedProd.unimedForm.markAsUntouched();        
  }

  inicializaForm() {
    this.getCadEmpresaEmpresa.codUnimed.setValue(this.getUnimedProd.codUnimed.value);
    this.getCadEmpresaEmpresa.descricao.setValue(this.getUnimedProd.descricao.value);    
  }

  verificaCampo(campo: string) {
    return AppUtil.verifyValidField(this.cadEmpresaForm, campo);
  }
  
  listarDepartamentos(){    
    this.departamentoService.listarDepartamentos().subscribe(outDep=>{   
      console.log(outDep);      
    });
  }

  buscarDepartamento(){
    let depar = new Departamento();
    depar.id = 1;
    depar.descricao = 'DEPARTAMENTO';
    depar.telefone = '29830429384098';
    depar.observacao = 'aksjdhakshdkahksjdh';
    this.departamentoService.buscarDepartamento(depar).subscribe(outDep=>{ 
      console.log(outDep);      
    });
  }

  get getCadEmpresaEmpresa(){ return this.cadEmpresaForm.controls; }
  get getUnimedProd(){ return this.unimedProd.unimedForm.controls; }  

}
