import { AppUtil } from './../../../../../../libs/util/src/lib/app.util';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'

@Component({
  selector: 'workspace-apps-filtro-empresas',
  templateUrl: './filtro-empresas.component.html',
  styleUrls: ['./filtro-empresas.component.css']
})
export class FiltroEmpresasComponent implements OnInit {

  filtroEmpresaform: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.filtroEmpresaFormGroup();
  }

  filtroEmpresaFormGroup() {
    this.filtroEmpresaform = this.formBuilder.group({
      codUnimed:        [null, [Validators.required]],
      descricao:        [null, [Validators.required]],
      codEmpresa:       [null, [Validators.required]],
      nomeFantasia:     [null, [Validators.required]],
      razaoSocial:      [null, [Validators.required]],
      cnpj:             [null],
      cei:              [null]            
    });        
  }

  verificaCampo(campo: string) {
    return AppUtil.verifyValidField(this.filtroEmpresaform, campo);
  }

}
