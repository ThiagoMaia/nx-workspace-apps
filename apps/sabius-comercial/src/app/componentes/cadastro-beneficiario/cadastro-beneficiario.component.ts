import { Unimed } from './../../../../../../libs/model/src/lib/unimed';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ConfirmationService, MessageService } from '../../../../../../node_modules/primeng/api';

const colsGridMeioContato = [
  { field: 'tipoEndereco', header: 'Tipo Endereço' },
  { field: 'tipoMeioContato', header: 'Tipo Contato' },
  { field: 'numeroContato', header: 'Contato' }
];

@Component({
  selector: 'app-cadastro-beneficiario',
  templateUrl: './cadastro-beneficiario.component.html',
  styleUrls: ['./cadastro-beneficiario.component.css']
})
export class CadastroBeneficiarioComponent implements OnInit {

  formDadosBenef: FormGroup;
  //formEnderecoBenef: FormGroup;
  //formMeioContatoBenef: FormGroup;
  formDialogMeioContato: FormGroup;
  abaCurrent: number = 0;
  pagMeioContato: number = 0;
  listaUnimeds: Unimed[];
  listUniSelecionados: Unimed[];

  exibeDialogMeioContato: Boolean = false;

  listaGridMeioContato: any[] = [];
  colsGridMeioCont: any[];
  meioContatoSelecionando: any[];

  constructor(private formBuilder: FormBuilder,
              private confirmationService: ConfirmationService,
              private messageService: MessageService) { }

  ngOnInit() {
    this.inicializeForm(); 
    
    this.colsGridMeioCont = colsGridMeioContato;    

    this.listaUnimeds=[
      {id:1, codUnimed:63, descricao:'UNIMED DE FORTALEZA'},
      {id:2, codUnimed:979, descricao:'UNIMED DE NATAL'},
      {id:3, codUnimed:123, descricao:'UNIMED DE MACEIO'},
      {id:4, codUnimed:39, descricao:'UNIMED DE GOIAS'}
    ];
    this.listUniSelecionados=[];

    Object.keys(this.getFormDadosBenef).forEach(campo=>{
      const controle = this.formDadosBenef.get(campo);
      controle.markAsDirty();    
    });
    Object.keys(this.getEndereco).forEach(campo=>{      
      const controle = this.formDadosBenef.controls.endereco.get(campo);      
      controle.markAsDirty(); 
    });
  }  

  inicializeForm() {

    this.formDadosBenef = this.formBuilder.group({
      nomeBenef: [null, [Validators.required]],
      
      endereco: this.formBuilder.group({
        nomeLogradouro: [null, [Validators.required]],
        tipoLogradouro: [null, [Validators.required]]
      }),

      meioContato: this.formBuilder.group({
        tipoEndereco: [null],
        descTipoMeioContato: [null],
        codTipoMeioContato: [null],
        contato: [null],
      }),

    });
    
    this.formDialogMeioContato = this.formBuilder.group({
      tipoEndereco: [null],
      descTipoMeioContato: [null],
      codTipoMeioContato: [null],
      contato: [null],
    });
  }

  adicionarMeioContato() {    
    let novoMeioContato =
      {tipoEndereco: this.getFormDadosBenef.meioContato.get('tipoEndereco').value,
       tipoMeioContato: this.getFormDadosBenef.meioContato.get('codTipoMeioContato').value,
       numeroContato: this.getFormDadosBenef.meioContato.get('contato').value
      };
    
    this.getFormDadosBenef.meioContato.reset();
    
    const arrMeioContato = [...this.listaGridMeioContato];   
    arrMeioContato.push(novoMeioContato);    
    this.listaGridMeioContato = arrMeioContato;

    //this.listaGridMeioContato.push(novoMeioContato);
    this.atualizaPagMeioContato();
  }

  deleteMeioContato(row) {
    this.confirmationService.confirm({
        message: 'Deseja realmente deletar o registro?',
        accept: () => {
           const index = this.listaGridMeioContato.indexOf(row);
           this.listaGridMeioContato = this.listaGridMeioContato.filter((val, i) => i !== index);
           this.atualizaPagMeioContato();
        }
    });
  }

  alterarContatoSelecionado(row) {
    this.formDialogMeioContato.get('tipoEndereco').setValue(row['tipoEndereco']);
    this.formDialogMeioContato.get('codTipoMeioContato').setValue(row['tipoMeioContato']);
    this.formDialogMeioContato.get('contato').setValue(row['numeroContato']);    
    this.meioContatoSelecionando = row;    
    this.exibeDialogMeioContato = true;
  }

  alterarMeioContato() {
    let meioContatoAlterado =
      {tipoEndereco: this.formDialogMeioContato.get('tipoEndereco').value,
       tipoMeioContato: this.formDialogMeioContato.get('codTipoMeioContato').value,
       numeroContato: this.formDialogMeioContato.get('contato').value
      };

    let arrMeioContato = [...this.listaGridMeioContato];    
    arrMeioContato[this.listaGridMeioContato.indexOf(this.meioContatoSelecionando)] = meioContatoAlterado;
    this.listaGridMeioContato = arrMeioContato;
    this.exibeDialogMeioContato = false;
  }

  atualizaPagMeioContato() {
    this.pagMeioContato = 0;
  }

  closeDialogMeioContato() {
    this.exibeDialogMeioContato = false;
  }

  eventBtnProximo(aba: number) {    
    if(this.validarAbaCurrent(aba+1)) {
      this.abaCurrent = (this.abaCurrent === 2) ? 0 : this.abaCurrent + 1;
    }
  }

  eventBtnAnterior() {
    this.abaCurrent = (this.abaCurrent === 0) ? 2 : this.abaCurrent - 1;    
  }

  eventAbaCurrent(event) {    
    if(this.validarAbaCurrent(event.index)) {
      this.abaCurrent = event.index;
    }
  }

  validarAbaCurrent(abaCurrent: number): boolean {
    let valid = false;    
    switch(abaCurrent) {
      case 0: { //Aba Dados Empresa       
        valid = this.validarAbaDadosEmpresa();        
        break; 
      }      
      case 1: { //Aba Endereco        
        valid = this.validarAbaDadosEmpresa();        
        break; 
      }
      case 2: { //Aba Meio Contato
        valid = this.validarAbaEndereco();        
        break; 
      } 
      default: {          
          break; 
      }      
    }
    return valid;
  }

  validarAbaDadosEmpresa() {
    console.log('DadosEmpresa validado.');
    return true;
  }

  validarAbaEndereco() {
    console.log('Endereco validado.');
    return true;
  }

  validarAbaMeioContato() {
    console.log('MeioContato validado.');
    return true;
  }

  get getFormDadosBenef(){return this.formDadosBenef.controls}
  get getEndereco(){return this.formDadosBenef.controls.endereco.value}

  addMessageSuccess(titulo: string, mensagem: string) {
    this.messageService.add({severity: 'success', summary: titulo, detail: mensagem});
  }

}
