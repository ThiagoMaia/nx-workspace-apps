import { HttpClient, HttpHeaders, HttpRequest, HttpParams } from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../../../../../node_modules/primeng/api';

@Component({
  selector: 'workspace-apps-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  uploadUrl: string;
  url: any;
 
  //uploadedFiles: File[] = [];

  uploadedFiles: any[] = [];

  //constructor(private messageService: MessageService) {}
  constructor(private http: HttpClient, private messageService: MessageService) {
    this.uploadUrl = 'http://localhost:8181/api/file/upload';
  }

  ngOnInit() {
  }

  dealWithFiles(event) {
    console.log(event);
  }

  onBeforeSendFile(event) {
    console.log(event);
    console.log('onBeforeSendFile()');    
  }
  onUploadFile(event) {
    this.onUploadTeste(event);
    console.log('onUploadFile()');    
  }
  onErrorFile(event) {
    console.log(event);
    console.log('onErrorFile()');  
  }
  onBeforeUploadFoto(event) {
    console.log(event);
    console.log('onBeforeUploadFoto()');    
  }

  onUploadTeste(event) {

    if(event.files.length === 0) {  
      this.messageService.add({severity: 'info', summary: 'File Upload', detail: 'Selecione pelo menos um arquivo.'});
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    
    console.log(this.url);
    reader.onload = (_event) => {
      this.url = reader.result; /**event.target.result; */
      console.log(this.url);
    }

    const input = new FormData();
    for(const file of event.files) {
      input.append("file", file, file.name);    
    }

    const params = new HttpParams();
    
    const httpOptions = {
      reportProgress: true, 
      params: params,      
      headers: new HttpHeaders({        
        'Accept': undefined,
        'Content-Type': undefined        
      })      
    };

    this.http.post(this.uploadUrl, input, httpOptions)
     .subscribe(res=>{
       //console.log(res);
       this.messageService.add({severity: 'info', summary: 'File Uploaded', detail: 'Upload de arquivo realizado.'});
      });    
  }

  onUpload(event) {

    if(event.files.length == 0) {    
      this.messageService.add({severity: 'info', summary: 'File Upload', detail: 'Selecione pelo menos um arquivo.'});
      return;
    }
    
    for(let file of event.files) {
        this.uploadedFiles.push(file);
    }

    const fileToUpload: File = event.files[0];
    const input = new FormData();    
    input.append("file", fileToUpload, fileToUpload.name);    

    const params = new HttpParams();    

    const httpOptions = {
      reportProgress: true, 
      params: params,      
      headers: new HttpHeaders({        
        'Accept': undefined,
        'Content-Type': undefined        
      })      
    };

    this.http.post(this.uploadUrl, input, httpOptions)
     .subscribe(res=>{
       //console.log(res);
       this.messageService.add({severity: 'info', summary: 'File Uploaded', detail: 'Upload de arquivo realizado.'});
      });    
  }



  myUploader(event): void {

    if(event.files.length == 0) {      
      console.log('selecione pelo menos um arquivo.');
      return;
    }

    const fileToUpload: File = event.files[0];
    const input = new FormData();    
    input.append("file", fileToUpload, fileToUpload.name);
    console.log(`arquivo selecionado: ${fileToUpload.name}`, fileToUpload);

    /*teste*/
    /*
    const headers = new Headers();         
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');*/  
    
    /*
    const req = new HttpRequest('POST', this.uploadUrl, input, {
      reportProgress: true,
      responseType: 'text',
      //params: params,
      headers: new HttpHeaders({        
        'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
        'Content-Disposition': 'form-data; name="file"; filename="02 Exercicio_TPC_IP(Aluno Thiago Weslley).jpg"'        
      })
    });*/    

    /*
    const headers = new HttpHeaders({ 
      'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
      'Content-Disposition': 'form-data; name="file"; filename="02 Exercicio_TPC_IP(Aluno Thiago Weslley).jpg"'
    });*/
    
    let params = new HttpParams();
    //params.set('file', fileToUpload.name);

    const httpOptions = {
      reportProgress: true, 
      params: params,
      //contentType: 'false',
      //mimeType: 'multipart/form-data',           
      headers: new HttpHeaders({        
        'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundaryhB1yijDA4lxaPjSx',
        //'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
        //'Content-Disposition': 'form-data; name="myFile[]"; filename="eu.jpeg"',
        //'Accept': 'application/json'
      })      
    };

    this.http.post(this.uploadUrl, input, httpOptions)
     .subscribe(res=>{console.log(res);})
     console.log('upload de arquivo realizado.');
    /*
    this.http.request(req).subscribe(res=>{
      console.log(res);
    });*/
    /*teste*/

    /*
    this.http.post(this.uploadUrl, input, httpOptions).subscribe(res=>{
      console.log(res);
    });*/
  }

  // upload completed event   
  onUpload2(event): void {
    for (const file of event.files) {
      this.uploadedFiles.push(file);
    }
    console.log('arquivo adicionado.');
  }

  onBeforeSend(event): void {
    console.log('usuario está enviado arquivo.');
    //event.xhr.setRequestHeader('Authorization', 'Bearer ' + abp.auth.getToken());   
  }

  /*
  onUpload(event) {
    console.log(event);
    for(let file of event.files) {
        this.uploadedFiles.push(file);
        console.log('chamou');
    }
    this.messageService.add({severity: 'info', summary: 'File Uploaded', detail: 'Upload Realizado com Sucesso!'});
  }*/
}
