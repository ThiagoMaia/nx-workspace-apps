import { AuthGuardService } from 'libs/core/src/lib/authentication/auth-guard.service';
import { LoginComponent } from 'libs/login/src/lib/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DefaultLayoutComponent } from './containers';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
    canActivate: [AuthGuardService]
  },
  {
    path: 'login',
    component: LoginComponent    
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuardService],
    data: {
      title: 'Home'
    },
    children: [      
      {
        path: 'dashboard',
        loadChildren: './componentes/dashboard/dashboard.module#DashboardModule'
      },      
      {
        path: 'beneficiarios',
        loadChildren: './modulos/beneficiarios/beneficiarios.module#BeneficiariosModule'
      },
      {
        path: 'empresas',
        loadChildren: './modulos/empresas/empresas.module#EmpresasModule'
      },     
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
