import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({  
  selector: 'body',
  template: `<ng4-loading-spinner [loadingText]="'Aguarde...'"></ng4-loading-spinner> 
             <router-outlet></router-outlet>`
})
export class AppComponent implements OnInit {
  constructor(private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }
}
