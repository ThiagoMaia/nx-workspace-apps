import { BeneficiariosRoutingModule } from './beneficiarios-routing.module';
import { PrimengModule } from '@workspace-apps/primeng';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastroBeneficiarioComponent } from '../../componentes/cadastro-beneficiario/cadastro-beneficiario.component';
import { UnimedProdModule } from '@workspace-apps/unimed-prod';
import { CoreModule } from '@workspace-apps/core';
import { FileUploadComponent } from '../../componentes/file-upload/file-upload.component';

@NgModule({
  imports: [
    CommonModule,
    BeneficiariosRoutingModule,
    PrimengModule,
    UnimedProdModule,
    CoreModule
  ],
  declarations: [
    CadastroBeneficiarioComponent,
    FileUploadComponent
  ]
})
export class BeneficiariosModule { }
