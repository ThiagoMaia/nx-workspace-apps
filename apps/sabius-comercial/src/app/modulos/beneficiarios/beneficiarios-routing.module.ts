import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CadastroBeneficiarioComponent } from '../../componentes/cadastro-beneficiario/cadastro-beneficiario.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Beneficiário'
    },
    children: [      
      {
        path: 'cadastrobeneficiario',
        component: CadastroBeneficiarioComponent,
        data: {
          title: 'Cadastro Beneficiário'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BeneficiariosRoutingModule {}