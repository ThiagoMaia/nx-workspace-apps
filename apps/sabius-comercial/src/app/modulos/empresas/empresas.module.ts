import { UppercaseDirective } from './../../../../../../libs/diretivas/src/lib/uppercase/uppercase.directive';
import { UtilModule } from '@workspace-apps/util';
import { UnimedProdModule } from './../../../../../../libs/unimed-prod/src/lib/unimed-prod.module';
import { EmpresaModule } from './../../../../../../libs/empresa/src/lib/empresa.module';
import { EmpresasRoutingModule } from './empresas-routing.module';
import { PrimengModule } from '@workspace-apps/primeng';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { CadastroEmpresaComponent } from '../../componentes/cadastro-empresa/cadastro-empresa.component';
import { TabelaCadastroEmpresaComponent } from '../../componentes/cadastro-empresa/tabela-cadastro-empresa/tabela-cadastro-empresa.component';
import { FiltroEmpresasComponent } from '../../componentes/filtro-empresas/filtro-empresas.component';

@NgModule({
  imports: [
    CommonModule,
    EmpresasRoutingModule,
    PrimengModule,
    UnimedProdModule,
    EmpresaModule,
    UtilModule    
  ],
  declarations: [
    CadastroEmpresaComponent,
    TabelaCadastroEmpresaComponent,
    UppercaseDirective,
    FiltroEmpresasComponent
  ],
  providers: [
    DatePipe
  ]
})
export class EmpresasModule { }
