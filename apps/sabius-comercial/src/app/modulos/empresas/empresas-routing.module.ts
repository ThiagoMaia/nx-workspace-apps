import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CadastroEmpresaComponent } from '../../componentes/cadastro-empresa/cadastro-empresa.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Empresas'
    },
    children: [      
      {
        path: 'cadastroempresa',
        component: CadastroEmpresaComponent,
        data: {
          title: 'Cadastro Empresa'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpresasRoutingModule {}