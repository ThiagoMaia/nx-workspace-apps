import { Router } from '@angular/router';
import { AuthService } from 'libs/core/src/lib/authentication/auth.service';
import { Component, Input } from '@angular/core';
import { navItems } from '../../_nav';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent {
  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;
  user: string = '';

  constructor(private authService: AuthService, private router: Router) {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true
    });

    this.user = this.authService.getUserLogged();
  }

  rerirectLogout() {
    this.authService.logout();    
    this.router.navigate(['/login']);
  }
}
