import { PrimengModule } from 'libs/primeng/src/lib/primeng.module';
import { CoreModule } from '@workspace-apps/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, /*LOCALE_ID*/ } from '@angular/core';
import { AppRoutingModule } from './app.routing';
import { LoginModule } from '@workspace-apps/login';
import { JwtModule } from '@auth0/angular-jwt';
import { LocationStrategy, 
         HashLocationStrategy, 
         //registerLocaleData, 
         /*DatePipe*/ } from '@angular/common';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
//import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

/*
import localePt from '@angular/common/locales/pt';
import localePtExtra from '@angular/common/locales/extra/pt';
registerLocaleData(localePt, 'pt', localePtExtra);*/

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';
import { DefaultLayoutComponent } from './containers';

const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    JwtModule,  
    LoginModule,
    CoreModule,
    Ng4LoadingSpinnerModule.forRoot()    
  ],
  declarations: [
    AppComponent,    
    ...APP_CONTAINERS
  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  }],
  /*
  providers: [{
    provide: [
      LocationStrategy 
      DatePipe, 
      {LOCALE_ID, useValue: 'pt'}
    ],
    useClass: HashLocationStrategy    
  }],*/  
  bootstrap: [ AppComponent ]
})
export class AppModule { }
