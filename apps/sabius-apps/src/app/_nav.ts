export const navItems = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    title: true,
    name: 'Theme'
  },
  {
    name: 'Cadastro Beneficiário',
    url: '/theme/cadastrobeneficiario',
    icon: 'icon-pencil'
  }  
];
