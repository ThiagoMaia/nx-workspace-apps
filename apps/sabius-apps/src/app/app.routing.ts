import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DefaultLayoutComponent } from './containers';

export const routes: Routes = [  
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },  
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [      
      {
        path: 'dashboard',        
        loadChildren: './componentes/dashboard/dashboard.module#DashboardModule'
      },        
      {
        path: 'theme',
        loadChildren: './componentes/theme/theme.module#ThemeModule'
      }          
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
