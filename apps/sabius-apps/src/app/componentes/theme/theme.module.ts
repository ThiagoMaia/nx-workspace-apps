import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ColorsComponent } from './colors.component';
import { TypographyComponent } from './typography.component';

import { ThemeRoutingModule } from './theme-routing.module';

import { CadastroBeneficiarioComponent } from '../cadastro-beneficiario/cadastro-beneficiario.component';
import { PrimengModule } from 'libs/primeng/src/lib/primeng.module';

@NgModule({
  imports: [
    CommonModule,
    ThemeRoutingModule,
    PrimengModule
  ],
  declarations: [
    ColorsComponent,
    TypographyComponent,
    CadastroBeneficiarioComponent
  ]
})
export class ThemeModule { }
