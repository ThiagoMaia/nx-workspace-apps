import { Directive, HostListener, Renderer2, ElementRef } from '@angular/core';

@Directive({  
  selector: '[uppercase]'
})
export class UppercaseDirective {

  @HostListener('keyup') toUppercase() {    
    this.el.nativeElement.value=this.el.nativeElement.value.toUpperCase();    
  }

  constructor(private rend: Renderer2, private el: ElementRef){}

}
