import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { ToolbarModule } from 'primeng/toolbar';
import { SplitButtonModule } from 'primeng/splitbutton';
import { CardModule } from 'primeng/card';
import { PanelModule } from 'primeng/panel';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ChartModule } from 'primeng/chart';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { InputMaskModule } from 'primeng/inputmask';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import { KeyFilterModule } from 'primeng/keyfilter';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { TabViewModule } from 'primeng/tabview';
import { PickListModule } from 'primeng/picklist';
import { FileUploadModule } from 'primeng/fileupload';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';

const DeclaretionModules: any[]=[
  FormsModule,
  ReactiveFormsModule,
  InputTextModule,
  ButtonModule,
  TableModule,
  DialogModule,
  ToolbarModule,
  SplitButtonModule,
  CardModule,
  PanelModule,
  MessagesModule,
  MessageModule,
  ChartModule,
  AutoCompleteModule,
  InputMaskModule,
  RadioButtonModule,
  CalendarModule, 
  CheckboxModule, 
  DropdownModule, 
  KeyFilterModule, 
  InputTextareaModule,
  TabViewModule,
  PickListModule,
  FileUploadModule,
  ToastModule,
  ConfirmDialogModule
];

@NgModule({
  imports: [
    CommonModule,
    DeclaretionModules
  ],
  exports: [
    DeclaretionModules
  ]
})
export class PrimengModule {}
