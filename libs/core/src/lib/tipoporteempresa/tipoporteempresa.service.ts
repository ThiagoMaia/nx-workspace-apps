import { TipoPorteEmpresa } from './../../../../model/src/lib/tipoporteempresa';
import { APP_HOST } from '@workspace-apps/util';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TipoPorteEmpresaService {

  constructor(private http: HttpClient) { }

  listarTipoPorteEmpresa(): Observable<TipoPorteEmpresa[]> {
    return this.http.get<TipoPorteEmpresa[]>(`${APP_HOST}/porteEmpresa`);
  }

}
