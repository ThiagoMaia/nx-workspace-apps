import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Observable } from 'rxjs';
import { APP_HOST } from '@workspace-apps/util';
import { Empresa } from './../../../../model/src/lib/empresa';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ArquivoGeraRel } from '../../../../model/src/lib/arquivogerarel';

@Injectable({
  providedIn: 'root'
})
export class EmpresaService {

  constructor(private http: HttpClient,
              private spinnerService: Ng4LoadingSpinnerService) { }

  buscarPorEmpresa(empresa: Empresa): Observable<Empresa[]> {
    let param = new HttpParams()
    param = param.append('codUnimed', `${empresa.codUnimed}`);
    param = param.append('codEmpresa', `${empresa.codEmpresa}`);
    const options = { params: param };
    return this.http.get<Empresa[]>(`${APP_HOST}/empresa`, options);
  }

  buscarEmpresaPorRazaoSocial(razaoSocial: string): Observable<Empresa[]> {
    let param = new HttpParams();
    param = param.append('?q', `${razaoSocial}`);
    const options = { params: param };
    return this.http.get<Empresa[]>(`${APP_HOST}/empresa`, options);        
  }

  listarEmpresas(): Observable<Empresa[]> {
    return this.http.get<Empresa[]>(`${APP_HOST}/empresa`);
  }

  salvar(empresa: Empresa): Observable<Empresa> {        
    return this.http.post<Empresa>(`${APP_HOST}/empresa`, empresa);
  }

  alterar(empresa: Empresa): Observable<Empresa> {    
    return this.http.put<Empresa>(`${APP_HOST}/empresa/${empresa.id}`, empresa);
  }

  deletar(empresa: Empresa): Observable<Empresa> {
    return this.http.delete<Empresa>(`${APP_HOST}/empresa/${empresa.id}`);    
  }

  gerarRelEmpresa() {    
    return this.http.get<ArquivoGeraRel>(`${APP_HOST}/arquivogerarel`);   
  }

}
