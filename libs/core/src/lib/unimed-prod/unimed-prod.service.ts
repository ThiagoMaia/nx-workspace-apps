import { ErrorHandler } from './../errorhandle';
import { Unimed } from '../../../../model/src/lib/unimed';
import { AuthService } from '../authentication/auth.service';
import { Injectable } from '@angular/core';
import { APP_HOST } from '@workspace-apps/util';
import { HttpClient, HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UnimedProdService {

  constructor(private authService: AuthService, private http: HttpClient) { }

  listarUnimeds() {        
    return this.http.get<Unimed[]>(`${APP_HOST}/unimeds`).toPromise()
      .then((unimeds: Unimed[]) => {
        return unimeds;
      });   
  }

  findByCodUnimed(codUnimed: number): Observable<Unimed[]> {
    let param = new HttpParams()
    param = param.append('codUnimed', `${codUnimed}`);        
    const options = { params: param };    
    return this.http.get<Unimed[]>(`${APP_HOST}/unimeds`, options)
      .pipe(
        catchError(ErrorHandler.handleError)
      );
  }

}
