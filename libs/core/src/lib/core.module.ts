import { DepartamentoService } from './departamento/departamento.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';

import { EmpresaService } from './empresa/empresa.service';
import { AuthService } from './authentication/auth.service';
import { AuthGuardService } from './authentication/auth-guard.service';
import { UnimedProdService } from './unimed-prod/unimed-prod.service';
import { TipoPorteEmpresaService } from './tipoporteempresa/tipoporteempresa.service';
import { TokenInterceptorService } from './authentication/token-interceptor.service';
import { NotificationService } from './notification/notification.service';
import { MessageService, ConfirmationService } from '../../../../node_modules/primeng/api';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ToastrModule.forRoot({
      //configuração modulo de notification
      positionClass: 'toast-top-right',
      closeButton: true,
      preventDuplicates: true,
      resetTimeoutOnDuplicate: true,
      progressBar: true,
      progressAnimation:'increasing'
      //configuração modulo de notification
    })
  ],  
  providers: [
    AuthService,
    AuthGuardService,
    UnimedProdService,
    EmpresaService,
    TipoPorteEmpresaService,
    NotificationService,
    DepartamentoService,
    MessageService,    
    ConfirmationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }    
  ]  
})
export class CoreModule {}
