import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private toastr: ToastrService) { }

  showSuccess(titulo: string, message: string) {
    titulo = this.isVerifyTitle(titulo);
    this.toastr.success(message, titulo);
  }

  showError(titulo: string, message: string) {
    titulo = this.isVerifyTitle(titulo);
    this.toastr.error(message, titulo)
  }

  showWarning(titulo: string, message: string) {
    titulo = this.isVerifyTitle(titulo);
    this.toastr.warning(message, titulo);
  }

  showInfo(titulo: string, message: string) {
    titulo = this.isVerifyTitle(titulo);
    this.toastr.info(message, titulo);
  }

  isVerifyTitle(titulo: string): string {
    if(titulo === undefined || titulo === '') {
      return titulo = 'Mensagem';
    } else {
      return titulo;
    }
  }
  
}
