import { Router } from '@angular/router';
import { APP_HOST } from '@workspace-apps/util';
import { Injectable } from '@angular/core';
import { Usuario } from '@workspace-apps/model';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  usuario: Usuario[]=[];
  returnUrl: string;
  errorLogin: boolean;  

  constructor(private http: HttpClient, private router: Router) {
    this.returnUrl = '/';
    this.errorLogin = false;
  }

  authenticateUser(currentUser: Usuario) {
    let param = new HttpParams()
    param = param.append('codUsuario',`${currentUser.codUsuario}`)
    param = param.append('senha',`${currentUser.senha}`);
        
    this.http.get<Usuario[]>(`${APP_HOST}/usuario`, {params: param}).subscribe(user => {                        

        if(user[0] !== undefined 
            && user[0].codUsuario == currentUser.codUsuario 
            && user[0].senha == currentUser.senha){
            localStorage.setItem('isLogged', "true");
            localStorage.setItem('codUsuario', user[0].codUsuario);            
            localStorage.setItem('token', user[0].token);
            this.router.navigate([this.returnUrl]);                        
            
            this.errorLogin = false;
            if(!this.isAuthenticated()) {
                this.errorLogin = true;
            }         
        } else {
            this.logout();
            this.errorLogin = true;                   
        }
    });
  }

  getErrorLogin(): Observable<boolean> {
      return new Observable<boolean>(obs => {
          setTimeout(()=>{
            obs.next(this.errorLogin);
          },1000);
      });      
  }

  isAuthenticated(): boolean {      
    if(this.getToken() === null
        || this.getToken() === '') {
        return false;
    }
    
    const helper = new JwtHelperService();
    const dateToken = helper.getTokenExpirationDate(this.getToken());    
    if(dateToken === undefined
        || dateToken === null) {            
        return false;
    } else {
        if(!helper.isTokenExpired(this.getToken())) {            
            return true;
        } else {            
            return false;
        }
    }    
    //return (dateToken.valueOf() >= new Date().valueOf());
    return false;
  }

  logout(): void {
      localStorage.setItem('isLogged', "false");
      localStorage.removeItem('codUsuario');                                           
      localStorage.removeItem('token');
  }

  getToken(): string {
      return localStorage.getItem('token');
  }

  getUserLogged(): string {
      let user = localStorage.getItem('codUsuario');
      if(user !== null && user !== '') {
          return localStorage.getItem('codUsuario').toUpperCase();
      } else {
          return '';
      }        
  }

}
