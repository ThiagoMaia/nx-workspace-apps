import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from 'libs/core/src/lib/authentication/auth.service';
import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  returnUrl: string;

  constructor(private injector: Injector, private router: Router) {
    this.returnUrl = '/';
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {    
    let authService = this.injector.get(AuthService);
    if(authService.getToken()!==null 
        && authService.getToken()!=='' 
        && !authService.isAuthenticated()){
      this.router.navigate([this.returnUrl]);
    }

    let contentType='';
    if(request.headers.has('Content-Type')){
      contentType = request.headers.get('Content-Type');
    } else {
      contentType = 'application/json';
    }
    
    request = request.clone({
      setHeaders: {
        'Authorization': `Bearer ${authService.getToken()}`,
        'Content-Type': contentType
      }
    });
    return next.handle(request);
  }
}
