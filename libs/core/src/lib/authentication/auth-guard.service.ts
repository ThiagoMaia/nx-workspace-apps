import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private authService: AuthService, private router : Router){}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let url: string = state.url;  
    return this.verifyLogin(url);
  }

  verifyLogin(url) : boolean {
    if(!this.authService.isAuthenticated()) {
        this.router.navigate(['/login']);
        return false;
    }
    else if(this.authService.isAuthenticated()) {
        return true;
    }
  }

  public isLogged(): boolean {
    let status = false;
    if( localStorage.getItem('isLogged') == "true"){
      status = true;
    }
    else {
      status = false;
    }
    return status;
  }
  
}
