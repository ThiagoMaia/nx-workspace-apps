import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

export class ErrorHandler {

    static handleError(error: HttpErrorResponse) {        
        //Erro do lado do cliente ou de rede.
        if (error.error instanceof ErrorEvent) {            
          console.error('Ocorreu um erro:', error.error.message);
        } else {
            //Erro do lado do backend.
            let errorMessage: string;
            const body = error.error;
            errorMessage = `url:${error.url}: status:${error.status} - ${error.statusText || ''} ${body}`;
          console.error(errorMessage);
        }        
        return throwError('Ocorreu um erro, por favor, tente novamente mais tarde.');
      };
}