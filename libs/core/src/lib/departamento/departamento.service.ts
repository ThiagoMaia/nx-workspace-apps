import { Departamento } from './../../../../model/src/lib/departamento';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class DepartamentoService {

  constructor(private http: HttpClient) { }

  listarDepartamentos() {
    return this.http.get<Departamento[]>(`http://localhost:8181/api/listar/departamentos`, httpOptions);                   
  }

  buscarDepartamento(depar: Departamento) {
    let depAux = depar.parseToJson(depar);     
    return this.http.post<Departamento[]>(`http://localhost:8181/api/buscar/departamento`, depAux, httpOptions);                   
  }

}
