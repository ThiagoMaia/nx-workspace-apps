import { FormGroup } from '@angular/forms';
export class AppUtil {

    public static configCalendarPtBr(): any {
        return {
            firstDayOfWeek: 0,
            dayNames: ["Domingo", "Segunda-Feira", "Terça-Feira", "Quarta-Feira", "Quinta-Feira", "Sexta-Feira", "Sábado"],
            dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
            dayNamesMin: ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
            monthNames: [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ],
            monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "Maio", "Jun", "Jul", "Aug", "Set", "Out", "Nov", "Dez" ],
            today: 'Hoje',
            clear: 'Limpar'
          };
    }

    public static verifyValidField(form: FormGroup, campo: string) {
        if(!form.get(campo).valid
           && (form.get(campo).dirty 
           || form.get(campo).touched)) {
            const controle = form.get(campo);
            controle.markAsDirty();
            return true;
        } else {
            return false;
        }
    }
}