/*host back end*/
export const APP_HOST = 'http://localhost:3000';

/*Regex: Email*/
export const EMAIL_PATTERN = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
/*Regex: Só Números*/
export const NUMBER_PATTERN = /^[0-9]*$/;
/*Regex: Só Letras*/
export const LETTERS_PATTERN = /^[A-Za-z ]*$/;
/*Regex: Só Letras com acentuação*/
export const LETTERS_FULL_PATTERN = /^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/;