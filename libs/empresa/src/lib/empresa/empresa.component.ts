import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Empresa } from './../../../../model/src/lib/empresa';
import { EmpresaService } from './../../../../core/src/lib/empresa/empresa.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { AppUtil } from '@workspace-apps/util';

@Component({
  selector: 'workspace-apps-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {

  empresaForm: FormGroup;  
  descEmpresa: string[];
  empresa: Empresa[]=[];

  constructor(private formBuilder: FormBuilder,
              private empresaService: EmpresaService) { }

  ngOnInit() {
    this.empresaFormGroup();
  }

  empresaFormGroup() {
    this.empresaForm = this.formBuilder.group({
      codEmpresa: [null, [Validators.required]],
      razaoSocial: [null, [Validators.required]]
    });

    this.getEmpresa.codEmpresa.valueChanges
     .pipe(
       debounceTime(500),
       distinctUntilChanged()
     ).subscribe(codEmpresa=>{
       if(codEmpresa !==undefined && codEmpresa !==''){
         this.empresaService.buscarPorEmpresa(this.empresaForm.value).subscribe(out=>{
           if(out[0] !== undefined) {
             this.getEmpresa.codEmpresa.setValue(out[0].codEmpresa);
             this.getEmpresa.razaoSocial.setValue(out[0].razaoSocial);
           }
         });
       } else {
        this.empresaForm.reset();
       }       
     });
    
    this.getEmpresa.razaoSocial.valueChanges
     .pipe(
       debounceTime(500),
       distinctUntilChanged()
     ).subscribe(razaoSocial=>{
       if(razaoSocial !==undefined && razaoSocial !==''){
         this.empresa.forEach(emp=>{
           if(emp.razaoSocial == razaoSocial){
             this.getEmpresa.codEmpresa.setValue(emp.codEmpresa);
           }
         });
       } else {
        this.empresaForm.reset();
       }       
    }); 
  }

  buscarEmpresa(event) {    
    this.empresaService.buscarEmpresaPorRazaoSocial(this.getEmpresa.razaoSocial.value)
     .subscribe((emp: Empresa[])=>{
        this.empresa = emp;        
        this.descEmpresa = this.filtrarEmpresa(event.query, emp);
     });
  }

  filtrarEmpresa(query, empresas: Empresa[]): string[] {
    let filtroEmpresas: string[]=[];
    for (let i = 0; i < empresas.length; i++) {
      const emp = empresas[i];
      if(emp.razaoSocial.toLowerCase().indexOf(query.toLowerCase()) == 0){
        filtroEmpresas.push(emp.razaoSocial);        
      }
    }
    return filtroEmpresas;
  }

  verificaCampo(campo: string) {
    return AppUtil.verifyValidField(this.empresaForm, campo);
  }

  get getEmpresa() {
    return this.empresaForm.controls
  };

}
