
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmpresaComponent } from './empresa/empresa.component';
import { PrimengModule } from '@workspace-apps/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@workspace-apps/core';
import { UtilModule } from '@workspace-apps/util';
        
@NgModule({
  imports: [
    CommonModule,
    PrimengModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    UtilModule
  ],
  declarations: [
    EmpresaComponent
  ],
  exports: [
    EmpresaComponent
  ]
})
export class EmpresaModule { }