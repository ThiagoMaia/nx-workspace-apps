import { UtilModule } from './../../../util/src/lib/util.module';
import { CoreModule } from './../../../core/src/lib/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnimedProdComponent } from './unimed-prod/unimed-prod.component';
import { PrimengModule } from '@workspace-apps/primeng';

@NgModule({
  imports: [
    CommonModule,
    PrimengModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    UtilModule
  ],
  declarations: [UnimedProdComponent],
  exports: [UnimedProdComponent]
})
export class UnimedProdModule {}
