import { UnimedProdService } from '../../../../core/src/lib/unimed-prod/unimed-prod.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Unimed } from '../../../../model/src/lib/unimed';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/catch';
import { AppUtil } from '@workspace-apps/util';

@Component({
  selector: 'workspace-apps-unimed-prod',
  templateUrl: './unimed-prod.component.html',
  styleUrls: ['./unimed-prod.component.css']
})
export class UnimedProdComponent implements OnInit {
  
  unimeds: Unimed[]=[];
  descUnimeds: string[];  
  unimedForm: FormGroup;

  constructor(private unimedProdService: UnimedProdService, 
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.unimedFormGroup();    
  }

  unimedFormGroup() {
    this.unimedForm = this.formBuilder.group({
      codUnimed: [null, [Validators.required]],
      descricao: [null, [Validators.required]]
    });

    this.getUnimed.codUnimed.valueChanges
      .pipe(
        debounceTime(500), 
        distinctUntilChanged()
      ).subscribe(codUnimed => {
        if(codUnimed !==undefined && codUnimed !=='') {   
          this.unimedProdService.findByCodUnimed(codUnimed).subscribe(out => {     
            if(out[0] !==undefined) {
              this.getUnimed.codUnimed.setValue(out[0].codUnimed);
              this.getUnimed.descricao.setValue(out[0].descricao);
            }
          });
        } else {
          this.unimedForm.reset();    
        }        
      });

    this.getUnimed.descricao.valueChanges
      .subscribe(descricao => {
        if(descricao !==undefined && descricao !=='') {
          this.unimeds.forEach(unimed => {
            if(unimed.descricao == descricao){
              this.getUnimed.codUnimed.setValue(unimed.codUnimed);
            }
          });
        } else {
          this.unimedForm.reset();
        }        
      });

  } 

  buscarUnimed(event) {    
    this.unimedProdService.listarUnimeds().then(unimeds => {
      this.unimeds = unimeds;
      this.descUnimeds = this.filtrarUnimeds(event.query, unimeds);      
    });
  }

  filtrarUnimeds(query, unimeds: Unimed[]): string[] {
    let filtroUnimeds : string[] = [];
    for(let i = 0; i < unimeds.length; i++) {
        let unimed = unimeds[i];
        if(unimed.descricao.toLowerCase().indexOf(query.toLowerCase()) == 0) {
          filtroUnimeds.push(unimed.descricao);
        }
    }
    return filtroUnimeds;
  }

  verificaCampo(campo: string) {
    return AppUtil.verifyValidField(this.unimedForm, campo);
  }

  get getUnimed() { return this.unimedForm.controls; }
}
