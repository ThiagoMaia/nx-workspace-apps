export class Departamento {
    id: number;
    descricao: string;
    telefone: string;
    observacao: string;

    parseToJson(dep: Departamento) {
        return JSON.stringify(dep);
    }

    parseToJsonObj(dep: Departamento) {
        return `{"departamento": ${JSON.stringify(dep)} }`;
    }
}