export class Empresa {
    id: number;
    codUnimed: number;
    codEmpresa: number;
    razaoSocial: string;
    nomeFantasia: string;
    cnpj: string;
    cei: string;
    dataCadastro: Date;
    dataExclusao: Date;
    tipoPfPj: string;
    tipoPorteEmpresa: string;    
}