import { LoginRoutingModule } from './login-routing.module';
import { PrimengModule } from '@workspace-apps/primeng';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { UtilModule } from '@workspace-apps/util';

@NgModule({
  imports: [
    CommonModule,
    PrimengModule,
    FormsModule,
    ReactiveFormsModule,
    LoginRoutingModule,
    UtilModule
  ],
  declarations: [
    LoginComponent
  ],
  exports: [
    LoginComponent,
    LoginRoutingModule
  ]
})
export class LoginModule { }
