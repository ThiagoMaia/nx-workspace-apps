import 'rxjs/add/operator/map';
import { Usuario } from '@workspace-apps/model';
import { AuthService } from '../../../../core/src/lib/authentication/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Observable } from 'rxjs';
import 'rxjs/add/operator/takeUntil';
import { AppUtil } from '@workspace-apps/util';

@Component({
  selector: 'workspace-apps-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  errorLogin = false;
  returnUrl: string;
  usuario: Observable<Usuario[]>;
  us: Usuario;
  usAux: Usuario[]=[];

  constructor(private formBuilder:FormBuilder, private router: Router, 
              public authService: AuthService) {}

  ngOnInit() {
    this.loginFormGroup();
    this.returnUrl = '/';
    this.authService.logout();  
  }

  loginFormGroup() {
    this.loginForm = this.formBuilder.group({
      codUsuario: [null, [Validators.required]],
      senha: [null, [Validators.required]]
    });
  }

  get getLogin(){return this.loginForm.controls}  

  login() {
    if(this.loginForm.valid) {
      let user = new Usuario();      
      user.codUsuario = this.getLogin.codUsuario.value;
      user.senha = this.getLogin.senha.value;      
      this.authService.authenticateUser(user);      
      this.authService.getErrorLogin().subscribe(errLogin => {
        this.errorLogin = errLogin;        
      });      
    } else {
      Object.keys(this.loginForm.controls).forEach(key => {
        let controls = this.loginForm.get(key);
        controls.markAsTouched();
      });
    }
  }

  verificaCampo(campo: string) {
    return AppUtil.verifyValidField(this.loginForm, campo);
  }

}
